Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Net
Imports System.Net.Sockets
Imports System.IO

Namespace Client_Socket
    Class Program
        Public Shared Sub Main(ByVal args As String())
            Try
                Console.WriteLine("That program can transfer file up to 100 mb.")
                Dim ipAddress As IPAddress() = Dns.GetHostAddresses("192.168.43.168")
                Dim ipEnd As IPEndPoint = New IPEndPoint(ipAddress(0), 5656)
                Dim clientSock As Socket = New Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.IP)
                clientSock.Connect(ipEnd)
                Dim clientData As Byte() = New Byte(5119999) {}
                Dim receivedPath As String = "D:/"
                Dim receivedBytesLen As Integer = clientSock.Receive(clientData)
                Dim fileNameLen As Integer = BitConverter.ToInt32(clientData, 0)
                Dim fileName As String = Encoding.ASCII.GetString(clientData, 4, fileNameLen)
                Console.WriteLine("Client:{0} connected & File {1} started received.", clientSock.RemoteEndPoint, fileName)
                Dim bWrite As BinaryWriter = New BinaryWriter(File.Open(receivedPath & fileName, FileMode.Append))
                bWrite.Write(clientData, 4 + fileNameLen, receivedBytesLen - 4 - fileNameLen)
                Console.WriteLine("File: {0} received & saved at path: {1}", fileName, receivedPath)
                bWrite.Close()
                clientSock.Close()
                Console.ReadLine()
            Catch ex As Exception
                Console.WriteLine("File Sending fail." & ex.Message)
            End Try
        End Sub
    End Class
End Namespace
